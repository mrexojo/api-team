# This file is a template, and might need editing before it works on your project.
FROM python:3.6-alpine

WORKDIR /usr/src/app

ENV FLASK_APP api.py
ENV FLASK_RUN_HOST 0.0.0.0

COPY ./src /usr/src/app

RUN pip install --no-cache-dir -r requirements.txt

#CMD python api.py
#CMD [ "gunicorn", "-b", ":5000", "api.py" ]

