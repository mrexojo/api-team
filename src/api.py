# Load libs
from flask import Flask, jsonify, request, Response
from flask_pymongo import PyMongo
from bson import json_util
from bson.objectid import ObjectId
import json

from werkzeug.security import generate_password_hash, check_password_hash

# Main vars
app = Flask(__name__)
app.config['MONGO_DBNAME'] = 'apiteam'
app.config['MONGO_URI'] = 'mongodb://mongodb:27017/apiteam'
mongo = PyMongo(app)


# app routes
# GET root API


@app.route('/')
def base():
    return Response(response=json.dumps({"API": "TeamCMP API | DevOps test", "Status": "UP", "Candidate": "@mrexojo"}),
                    status=200,
                    mimetype='application/json')

# GET User


@app.route('/user/<id>', methods=['GET'])
def get_user(id):

    user = mongo.db.users.find_one({'_id': ObjectId(id), })
    response = json_util.dumps(user)
    if user:
        return Response(response, status=200, mimetype='application/json')
    else:
        return not_found()

# POST User


@app.route('/user', methods=['POST'])
def create_user():
    username = request.json['username']
    email = request.json['email']
    password = request.json['password']
    pmail = mongo.db.users.find_one({'email': email})
    puser = mongo.db.users.find_one({'username': username})

    if puser or pmail:
        message = {
            'message': "User or email already exists"
        }
        response = jsonify(message)
        response.status_code = 403
        return response
    else:
        if username and email and password:
            hashed_password = generate_password_hash(password)
            id = mongo.db.users.insert(
                {'username': username, 'email': email, 'password': hashed_password})
            message = {
                '_id': str(id),
                'username': username,
                'email': email
            }
            response = jsonify(message)
            response.status_code = 201
            return response
        else:
            message = {
                'message': "username, email, and password are required"
            }
    response = jsonify(message)
    response.status_code = 400
    return response

# DELETE USER


@ app.route('/user/<id>', methods=['DELETE'])
def delete_user(id):

    pid = mongo.db.users.find_one({'_id': ObjectId(id)})
    if pid:
        mongo.db.users.delete_one({'_id': ObjectId(id)})
        message = {
            'message': 'User deleted', 'id': str(id)
        }
        response = jsonify(message)
        response.status_code = 200
        return response
    else:
        return not_found()

# Error Not found


@ app.errorhandler(404)
def not_found(error=None):
    message = {
        'message': 'User Not Found ' + request.url,
    }
    response = jsonify(message)
    response.status_code = 404
    return response

# Load Main


if __name__ == "__main__":
    app.run(debug=False)
